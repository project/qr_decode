<?php

namespace Drupal\qr_decode\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;

/**
 * Plugin implementation of the 'qr_decode_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "qr_decode_widget",
 *   label = @Translation("Qr code widget"),
 *   field_types = {
 *     "qr_decode_field_type"
 *   }
 * )
 */
class QrDecodeWidget extends ImageWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#alt_field_required'] = FALSE;
    $element['#alt_field'] = FALSE;
    return $element;
  }

  /**
   * Form API callback: Processes a image_image field element.
   *
   * Expands the image_image type to include the alt and title fields.
   *
   * This method is assigned as a #process callback in formElement() method.
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $item = $element['#value'];
    $item['fids'] = $element['fids']['#value'];
    $element['#theme'] = 'qr_image_widget';

    // Add the image preview.
    if (!empty($element['#files']) && $element['#preview_image_style']) {

      unset($element['alt_field_required']);
      $file = reset($element['#files']);
      $variables = [
        'style_name' => $element['#preview_image_style'],
        'uri' => $file->getFileUri(),
      ];

      // Determine image dimensions.
      if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
        $variables['width'] = $element['#value']['width'];
        $variables['height'] = $element['#value']['height'];
        $variables['data'] = $element['#value']['data'];
      }
      else {
        $image = \Drupal::service('image.factory')->get($file->getFileUri());
        if ($image->isValid()) {
          $variables['width'] = $image->getWidth();
          $variables['height'] = $image->getHeight();
          $qrDecode = new \QrReader($file->getFileUri(), \QrReader::SOURCE_TYPE_FILE, FALSE);
          // Return decoded text from QR Code.
          $item['data'] = $qrDecode->text();
          $variables['data'] = $item['data'];

        }
        else {
          $variables['width'] = $variables['height'] = NULL;
        }
      }

      $element['preview'] = [
        '#weight' => -10,
        '#theme' => 'image_style',
        '#width' => $variables['width'],
        '#height' => $variables['height'],
        '#style_name' => $variables['style_name'],
        '#uri' => $variables['uri'],
      ];

      // Store the dimensions in the form so the file doesn't have to be
      // accessed again. This is important for remote files.
      $element['width'] = [
        '#type' => 'hidden',
        '#value' => $variables['width'],
      ];
      $element['height'] = [
        '#type' => 'hidden',
        '#value' => $variables['height'],
      ];
      $element['data'] = [
        '#type' => 'textfield',
        '#title' => t('Decoded data'),
        '#default_value' => $item['data'],
        '#maxlength' => 1024,
        '#weight' => -11,
        '#value' => $variables['data'],
      ];
    }

    return parent::process($element, $form_state, $form);
  }

}
