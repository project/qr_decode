<?php

namespace Drupal\qr_decode\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'qr_decode_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "qr_decode_formatter",
 *   label = @Translation("QR Decode field formatter"),
 *   field_types = {
 *     "qr_decode_field_type"
 *   }
 * )
 */
class QrDecodeFormatterType extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $data = $item->data;
      $elements[$delta] = [
        '#theme' => 'qr_decode_formatter',
        '#data' => $data,
      ];
    }

    return $elements;
  }

}
